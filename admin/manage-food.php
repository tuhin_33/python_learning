<?php include('partials/menu.php') ?>

<div class="main-content">
    <div class="wrapper">
        <h1>Manage Food</h1>

        <br> <br> 

            <!-- Button to add Admin -->
            <a href="#" class="btn-primary">Add Food</a>


            <table class="tbl-full">
                <tr>
                    <th>S.H.</th>
                    <th>Full Name</th>
                    <th>Username</th>
                    <th>Actions</th>
                </tr>
                
                <tr>
                    <td>1.</td>
                    <td>Debjyoti Malik</td>
                    <td>debjyotiimalik</td>
                    <td>
                        <a href="#" class="btn-secondary">Update Admin</a> 
                        <a href="#" class="btn-danger">Update Admin</a> 
                    </td>
                </tr>

                <tr>
                    <td>2.</td>
                    <td>Debjyoti Malik</td>
                    <td>debjyotiimalik</td>
                    <td>
                        <a href="#" class="btn-secondary">Update Admin</a> 
                        <a href="#" class="btn-danger">Update Admin</a> 
                    </td>
                </tr>

                <tr>
                    <td>3.</td>
                    <td>Debjyoti Malik</td>
                    <td>debjyotiimalik</td>
                    <td>
                       <a href="#" class="btn-secondary">Update Admin</a> 
                       <a href="#" class="btn-danger">Update Admin</a> 
                    </td>
                </tr>

            </table>
    </div>
</div>

<?php include('partials/footer.php') ?>